import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    redirect: "login"
  },
  {
    path: "/login",
    name: "login",
    component: () => import("../views/Login.vue")
  },
  {
    path: "/page",
    name: "page",
    component: () => import("../views/Page/Page.vue"),
    children: [
      {
        path: "computing",
        name: "computing",
        component: () => import("../views/Page/Computing.vue")
      },

      {
        path: "information",
        name: "information",
        component: () => import("../views/Page/Information.vue")
      },
    ]
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;

