import { NotificationProgrammatic as Notification } from 'buefy'

function notification  (message, type ) {
  Notification.open({
    duration: 3000,
    position: 'is-top-left',
    message,
    type,
  });
};
export function successNotification (message = 'Some errors') {
  notification(message, 'is-success');
};

export function dangerNotification  (message = 'Some errors') {
  notification(message, 'is-danger');
};
